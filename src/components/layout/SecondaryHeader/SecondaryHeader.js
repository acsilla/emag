import React from 'react';
import './SecondaryHeader.scss';
import Octicon, { Grabber } from '@primer/octicons-react';
import { HelpIcon } from '../../icons/HelpIcon';

export function SecondaryHeader() {
  return (
    <div className="secondary-header">
      <nav className="navbar secondary-navbar">
        <div className="container">
          <div className="navbar-content">
            <ul className="navbar-left">
              <li className="navbar-item navbar-item-active"><Octicon icon={Grabber} className="icon" />Produse</li>
              <li className="navbar-item">Newsletter</li>
              <li className="navbar-item">Resigilate</li>
              <li className="navbar-item">Card cadou</li>
              <li className="navbar-item">Le-ai vazut la TV</li>
            </ul>
            <ul className="navbar-right">
              <li className="navbar-item"><Octicon icon={HelpIcon} className="icon" />eMAG Help</li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  )
};