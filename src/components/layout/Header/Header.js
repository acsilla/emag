import React from 'react';
import './Header.scss';
import { HeaderItem } from '../HeaderItem/HeaderItem';
import Octicon, { Search, Heart, MarkGithub, TriangleDown } from '@primer/octicons-react';
import { CartIcon } from '../../icons/CartIcon';

export function Header() {
  return (
    <header className="header">
      <nav className="navbar navbar-main adjust-on-modal">
        <div className="container">
          <div className="row navbar-inner">
            <div className="col-2 navbar-branding">
              <a className="navbar-brand" href="/"><img src={require("../../icons/logo.svg")} className="Logo" alt="logo"></img></a>
            </div>
            <div className="col-6 navbar-searchbox">
              <form action="/search" get="" noValidate>
                <div className="input-group searchbox-input">
                  <div className="form-control">
                    <input type="text" id="searchboxAutocomplete" className="searchbox-suggestion" autoComplete="off" />
                    <input type="search" id="searchboxTrigger" name="query" className="searchbox-main" placeholder="Ai libertatea să alegi ce vrei" autoComplete="off" />
                  </div>
                  <div className="input-group-btn">
                    <button className="btn btn-default searchbox-submit-button">
                      <span className="sr-only">Cauta</span>
                      <Octicon icon={Search} className="icon" verticalAlign='middle' />
                    </button>
                  </div>
                </div>
              </form>
            </div>
            <div className="col-4">
              <div className="row">
                <HeaderItem text="Contul meu" icon={MarkGithub} arrow={TriangleDown}>
                  <p className="list-title"> Salut, John Doe </p>
                  <div class="dropdown-divider"></div>
                  <ul className="dropdown-list">
                    <li>Comenzile mele</li>
                    <li>Cardurile mele</li>
                    <li>Favorite</li>
                    <div class="dropdown-divider"></div>
                    <li>Log out</li>
                  </ul>
                </HeaderItem>
                <HeaderItem text="Favorite" icon={Heart} arrow={TriangleDown}>
                  <p> Nu ai produse favorite </p>
                </HeaderItem>
                <HeaderItem text="Cosul meu" icon={CartIcon} arrow={TriangleDown}>
                </HeaderItem>
              </div>              
            </div>
          </div>
        </div>
      </nav>
    </header>
  );
}