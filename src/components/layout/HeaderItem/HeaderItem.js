import React from 'react';
import './HeaderItem.scss';
import Octicon from '@primer/octicons-react';

export function HeaderItem(props) {
  return (
    <div className="col-4 media header-item">
      <div className="media-body">
        <Octicon icon={props.icon} className="icon" />
        <p className="media-heading">{props.text}</p>
        <Octicon icon={props.arrow} className="arrow" />
        <div className="dropdown">{props.children}</div>
      </div>
    </div>
  )
};