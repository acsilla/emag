import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { Header } from './components/layout/Header/Header';
import { SecondaryHeader } from './components/layout/SecondaryHeader/SecondaryHeader';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Header />
        <SecondaryHeader />
      </header>
    </div>
  );
}

export default App;
